const express = require('express');
const json = express.json;

const app = express();

//configurar cabeceras http
app.use((req,res,next)=>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY,Origin,X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request');
	res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');
	next();
});
//Cargamos una ruta estatica que es la carpeta client
app.use('/',express.static('client',{redirect:false}));
//Rutas
const rindex = require('./routes/index');
const rtasks = require('./routes/tasks');

//Config
app.set('port', process.env.PORT || 3000);


app.use(json());

//Rutas
app.use(rindex);
app.use('/tasks', rtasks);
app.use('*',function (req,res,next) {
    res.sendFile(path.resolve('../client/index.js'));
});
module.exports = app;